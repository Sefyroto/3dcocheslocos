﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class OrbeHielo : MonoBehaviour
{
    public GameEventSTO congelarTiempo;
    private void OnTriggerEnter(Collider collider)
    {
        if(collider.tag == "Player")
        {
            congelarTiempo.Raise();
            Destroy(this.gameObject);
        }
    }
}
