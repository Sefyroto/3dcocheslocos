﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.SceneManagement;
using UnityEngine;

public class EmpezarCarrera : MonoBehaviour
{
    //esto es lo que hay en los botones del menu basicamente inicia el juego o sale de la aplicacion
   public void carrera()
    {
        SceneManager.LoadScene("juego");
    }
    public void salir()
    {
        Application.Quit();
    }
}
