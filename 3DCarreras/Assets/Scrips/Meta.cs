﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.SceneManagement;
using UnityEngine;

public class Meta : MonoBehaviour
{
    bool finCarrera = false;
    public GameEventSTO fin; 

    private void OnTriggerEnter(Collider collider)
    {
        //cuando la meta colisiona con el jugador se termina la carrera y lo lleba a otra escena
        if (collider.tag == "Player")
        {
            finCarrera = true;
            fin.Raise();
            SceneManager.LoadScene("Menu");
        }
    }
}
