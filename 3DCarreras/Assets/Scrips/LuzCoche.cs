﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LuzCoche : MonoBehaviour
{
    Light faroD, faroI, faroDL, faroIL, frenoD, frenoI;

    // Start is called before the first frame update
    void Awake()
    {
        faroD = transform.Find("FaroDerecho").gameObject.GetComponent<Light>();
        faroDL = transform.Find("FaroDerechoL").gameObject.GetComponent<Light>();
        faroI = transform.Find("FaroIzquierdo").gameObject.GetComponent<Light>();
        faroIL = transform.Find("FaroIzquierdoL").gameObject.GetComponent<Light>();
        frenoD = transform.Find("FrenoDerecho").gameObject.GetComponent<Light>();
        frenoI = transform.Find("FrenoIzquierdo").gameObject.GetComponent<Light>();
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKeyDown(KeyCode.L))
        {
            if (!faroD.enabled && !faroI.enabled && !faroDL.enabled && !faroIL.enabled)
            {
                //luzes cortas
                faroD.enabled=true;
                faroI.enabled=true;
            }
            else if (faroD.enabled && faroI.enabled)
            {
                //luzes largas
                faroD.enabled = false;
                faroI.enabled = false;
                faroDL.enabled = true;
                faroIL.enabled = true;
            }
            else
            {
                //luzes apagadas
                faroDL.enabled=false;
                faroIL.enabled=false;
            }
        }
        if (Input.GetKey(KeyCode.S))
        {
            //si pulsas S las luzes traseras se vuelven mas intensas
            frenoD.intensity = 3;
            frenoI.intensity = 3;
        }
        else
        {
            frenoD.intensity = 1;
            frenoI.intensity = 1;
        }
    }
}
