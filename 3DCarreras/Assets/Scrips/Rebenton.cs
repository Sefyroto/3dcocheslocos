﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Rebenton : MonoBehaviour
{
    private void OnTriggerEnter(Collider collision)
    {
        //cuando colisionan las ruedas con los pinchos desactivan las ruedas y las reactivan a los 3s con 2 coorrutinas
        if (collision.gameObject.tag == "Pinchos")
        {
            StartCoroutine(sinRuedas());
        }
    }

    private void sinRueda()
    {
        //quitar rueda
        this.transform.parent.GetComponent<WheelCollider>().enabled = false;
    }
    private void conRueda()
    {
        //poner rueda
        this.transform.parent.GetComponent<WheelCollider>().enabled = true;
    }

    IEnumerator sinRuedas()
    {
        sinRueda();
        yield return new WaitForSeconds(3f);
        conRueda();
    }
}
