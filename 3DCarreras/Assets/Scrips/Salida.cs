﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Salida : MonoBehaviour
{
    bool inicioCarrera = false;
    public GameEventSTO inicio;
    
    private void OnTriggerExit(Collider collider)
    {
        //mientras no salga de la plataforma inicial no empezara a contar el contador
        if (collider.tag == "Player" && !inicioCarrera)
        {
            inicioCarrera = true;
            inicio.Raise();
        }
    }
}
