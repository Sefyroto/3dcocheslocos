﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using UnityEngine;

public class Contador : MonoBehaviour
{
    int tiempo;
    public Text text;
    bool tiempoCorriendo = false;
    
    void Start()
    {
        tiempo = 0;
    }
    public void inicioContador()
    {
        //se inicia cuando sale del cuadrado inicial 
        tiempoCorriendo = true;
        StartCoroutine(crono());
    }

    public void pararContador()
    {
        //se para cuando se termina la carrera y cuando se coje un orbe 
        tiempoCorriendo = false;
        StopCoroutine(crono());
    }
    public void pararYReanudar()
    {
        //cuando cojes un orbe paras y reanudas el tiempo con una corrutina
        StartCoroutine(stopPlay());
    }
    IEnumerator crono()
    {
        while (tiempoCorriendo) {
            //cada segundo que pase se le suma uno al contador
            yield return new WaitForSeconds(1f);
            tiempo++;
            text.text = tiempo.ToString();
        }
    }
    IEnumerator stopPlay()
    {
        pararContador();
        yield return new WaitForSeconds(3f);
        inicioContador();
    }
}
