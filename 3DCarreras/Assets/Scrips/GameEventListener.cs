﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

/// <summary>
/// game event listener
/// </summary>
public class GameEventListener : MonoBehaviour
{
    [Tooltip("Event to register with")]
    public GameEventSTO Event;

    [Tooltip("Response to invoke when Event is raised")]
    public UnityEvent Response;

    private void OnEnable()
    {
        Event.RegisterListener(this);
    }

    private void OnDisable()
    {
        Event.UnregisterListener(this);
    }

    internal void OnEventRaised()
    {
        Response.Invoke();
    }

}
