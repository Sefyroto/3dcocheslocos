﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Respawn : MonoBehaviour
{
    Transform techo;
    Vector3 posicionRespawn;
    Quaternion rotacionRespawn;

    // Start is called before the first frame update
    void Start()
    {
        techo = transform;
        posicionRespawn = new Vector3(0, 2f, 0);
        rotacionRespawn = Quaternion.Euler(0, 0, 0);
    }

    // Update is called once per frame
    void Update()
    {
        //si presionas R el coche spawneara +2 en y, y en 0 0 0 de rotacion para ponerlo de pie si vuelca
        if (Input.GetKeyDown(KeyCode.R))
        {
            techo.position += posicionRespawn;
            techo.rotation = rotacionRespawn;
        }
    }
}
