﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// scriptable object for game events
/// </summary>
[CreateAssetMenu(fileName="Events")]
public class GameEventSTO : ScriptableObject
{
    private readonly List<GameEventListener> eventListeners = new List<GameEventListener>();
    
    public void Raise()
    {
        //llama a todos los que esten suscritos a los listeners corespondientes
        for(int i = eventListeners.Count -1; i >= 0; i--)
        {
            eventListeners[i].OnEventRaised();
        }
    }

    public void RegisterListener(GameEventListener listener)
    {
        //Añade un game event listener
        if (!eventListeners.Contains(listener))
        {
            eventListeners.Add(listener);
        }
    }

    public void UnregisterListener(GameEventListener listener)
    {
        //Quita un game event listener
        if (eventListeners.Contains(listener))
        {
            eventListeners.Remove(listener);
        }
    }
}
