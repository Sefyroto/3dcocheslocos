﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Movimiento : MonoBehaviour
{
    private const string horizontal = "Horizontal";
    private const string vertical = "Vertical";

    private float horizontalInput;
    private float verticalInput;
    private float currentSteerAngle;
    private float currentbreakForce;
    private bool isBreaking;
    private bool propulsion = false;
    

    [SerializeField] private float motorForce;
    [SerializeField] private float breakForce;
    [SerializeField] private float maxSteerAngle;

    [SerializeField] private WheelCollider frontLeftWheelCollider;
    [SerializeField] private WheelCollider frontRightWheelCollider;
    [SerializeField] private WheelCollider rearLeftWheelCollider;
    [SerializeField] private WheelCollider rearRightWheelCollider;

    [SerializeField] private Transform frontLeftWheelTransform;
    [SerializeField] private Transform frontRightWheeTransform;
    [SerializeField] private Transform rearLeftWheelTransform;
    [SerializeField] private Transform rearRightWheelTransform;
    private void Update()
    {
        GetInput();
        if (Input.GetKeyDown(KeyCode.LeftShift) && !propulsion)
        {
            StartCoroutine(nitro());
        }
    }

    private void FixedUpdate()
    {
        HandleMotor();
        HandleSteering();
        UpdateWheels();
    }
 
    private void GetInput()
    {
        //los 3 tipos de imput mas importantes, porque para mi el nitro es a parte
        horizontalInput = Input.GetAxis(horizontal);
        verticalInput = Input.GetAxis(vertical);
        isBreaking = Input.GetKey(KeyCode.Space);
    }

    private void HandleMotor()
    {
        //si se presiona un imput vertical todas las ruedas se moveran a esa direccion con la fuerza del motor "Las 4 ruedas tienen fuerza"
        frontLeftWheelCollider.motorTorque = verticalInput * motorForce;
        frontRightWheelCollider.motorTorque = verticalInput * motorForce;
        rearLeftWheelCollider.motorTorque = verticalInput * motorForce;
        rearRightWheelCollider.motorTorque = verticalInput * motorForce;
        if (isBreaking || verticalInput == 0)
        {
            currentbreakForce = breakForce;
        }
        else
        {
            currentbreakForce = 0f;
        }
        ApplyBreaking();
    }

    private void ApplyBreaking()
    {
        //Esto es en que ruedas se aplica el "Freno de mano"

        //frontRightWheelCollider.brakeTorque = currentbreakForce;
        //frontLeftWheelCollider.brakeTorque = currentbreakForce;
        rearLeftWheelCollider.brakeTorque = currentbreakForce;
        rearRightWheelCollider.brakeTorque = currentbreakForce;
    }

    private void HandleSteering()
    {
        //en resumen es que las ruedas delanteras puedan girar
        currentSteerAngle = maxSteerAngle * horizontalInput;
        frontLeftWheelCollider.steerAngle = currentSteerAngle;
        frontRightWheelCollider.steerAngle = currentSteerAngle;
    }

    private void UpdateWheels()
    {
        //las ruedas rotan tanto el collider como el transform de cada rueda
        UpdateSingleWheel(frontLeftWheelCollider, frontLeftWheelTransform);
        UpdateSingleWheel(frontRightWheelCollider, frontRightWheeTransform);
        UpdateSingleWheel(rearRightWheelCollider, rearRightWheelTransform);
        UpdateSingleWheel(rearLeftWheelCollider, rearLeftWheelTransform);
    }

    private void UpdateSingleWheel(WheelCollider wheelCollider, Transform wheelTransform)
    {
        //las ruedas rotan y dan el efecto de movimiento
        Vector3 pos;
        Quaternion rot;
        wheelCollider.GetWorldPose(out pos, out rot);
        wheelTransform.rotation = rot;
        wheelTransform.position = pos;
    }
    public IEnumerator nitro()
    {
        //Cuando pulsas el shift llamas a esta coorutina que te proporciona +1000 a la fuerza de las ruedas
        propulsion = true;
        motorForce += 1000;
        yield return new WaitForSeconds(5f);
        motorForce -= 1000;
        yield return new WaitForSeconds(25f);
        propulsion = false;
    }
}